/**

	INTRODUCCIÓN A LA PROGRAMACIÓN / FUNDAMENTOS DE PROGRAMACIÓN
	Curso 2011/2012

	Nombre: main.cpp
	Descripción: Implementación del programa principal
	Autor:	Profesores de las asignaturas
	Fecha:	03/11/2011

*/


#include <iostream>
#include <stdio.h>

#include "entorno.h"
#include "reloj.h"

using namespace std;


// -------------------------------------------------------------
// MÓDULO PRINCIPAL
// -------------------------------------------------------------
void PruebaEntorno(){
		  char         msg[40];
		  bool         salir     = false;
		  TipoTecla    tecla     = TNada;
		  bool         ganador   = false;
		  int          fila, col; //variables auxiliares para recorrer el tablero
		  int          n_filas, n_columnas; //dimensiones del tablero de juego
		  int 		   tiempo; //número de segundos que durará la partida
		  TReloj	   reloj;
		  int 			p;  //mide el tamaño de la barra que mide el tiempo
		  int mb; //Fichas a borrar
		  int com; //Comodin (Que no se utiliza hasta implementar la ampliación)
		  int ran; //Generacion de las fichas randomizadas o no (1 = cierto 0= falso)
		  int tab[MAX_NUM_FILAS][MAX_NUM_COLUMNAS]; // Estructura que almacena la configuracion inicial del tablero
		  int fichas;

		  //Inicializamos las variables citadas anteriomente desde el fichero de configuración

		  TEntornoCargarConfiguracion(n_filas,n_columnas,fichas,tiempo,mb,com,ran,tab);



		  // Iniciamos el entorno del juego, dibujando un tablero de 8*5
		  TEntornoIniciar (n_filas,n_columnas);

		  // creamos un reloj que hará que el juego dure 30 segundos
		  reloj = CrearReloj(tiempo);
	      //dibuja la barra incial que mide el tiempo
		  TEntornoTiempoInicio();
	      //dibuja la barra que mide el tiempo
	      ActualizarReloj(reloj,p,msg);
	      TEntornoMostrarMensaje(Zona1, msg);

	      sprintf(msg, "REFLEKT: Prueba del entorno");
	      TEntornoMostrarMensaje(Zona2, msg);
	      sprintf(msg, "Pulsa ESC para salir");
	      TEntornoMostrarMensaje(Zona3, msg);
	      sprintf(msg, "Puntuación: %d", 0);
		  TEntornoMostrarMensaje(Zona4, msg);

// Colocamos tres fichas en la zona superior del tablero
		  //Los dos primeros parametros son las coordenadas de la matriz
		  //El tercero es el color de la ficha
		   //el último parámetro es 0
		       TEntornoPonerCasilla(1, 1, 3,0) ;
		       TEntornoPonerCasilla(2, 2, 2,0) ;
		       TEntornoPonerCasilla(3, 3, 4,0) ;
		       TEntornoPonerCasilla(0, 0, 4,0) ;

// Colocamos tres fichas en la zona superior del tablero
	  //Los dos primeros parametros son las coordenadas de la matriz
	  //El tercero es el color de la ficha
	   //el último parámetro es 1 y asi se colocan en la matriz relfejada
	//Hay que tener en cuanta los calculos necesarios para poner la casilla en la matriz reflejada
		     TEntornoPonerCasilla(4, 3, 3,1) ;
		     TEntornoPonerCasilla(5, 2, 1,1) ;
		     TEntornoPonerCasilla(6, 1, 2,1) ;
		     TEntornoPonerCasilla(7, 3, 4,1) ;

		     sprintf(msg, "Parpadeo de una casilla");
		     TEntornoMostrarMensaje(Zona3, msg);
		     usleep(PAUSA); //para unos segundos la ejecución del programa
		     //Hace que parpadee la casilla colocada en la fila 3, columna 3, la cambia al color 3
		     TEntornoParpadearCasilla(3,3,3,0);


		     sprintf(msg, "Parpadeo de dos casillas");
		     //Muestra ell mensaje contenido en msg en la zonaN indicada dentro de la intefaz.
		     TEntornoMostrarMensaje(Zona3, msg);
		     usleep(PAUSA); //para unos segundos la ejecución del programa
		     //Los dos primeros argumentos corresponden a la coordenada primera casilla y las dos segundas
		     //a la segunda casilla. El penultimo argumento es el color al que parpadeara la primera casilla
		     //y el ultimo el color al que parpadeará la segunda casilla.(Si no son iguales a los originales los cambia.
		     TEntornoParpadearDosCasillas(1,1,2,2,3,2);

		     sprintf(msg, "Borrado de una casilla");
		     TEntornoMostrarMensaje(Zona3, msg);
		     usleep(PAUSA); //para unos segundos la ejecución del programa
		     //Borra de la interfaz la casilla que ocupa las coordenadas indicadas por los argumentos
		     TEntornoEliminarCasilla(6,1);

		     sprintf(msg, "Desplazamiento por el tablero");
		     TEntornoMostrarMensaje(Zona4, msg);

		  fila = 0;
		  col  = 0;
		  //Los argumentos indican una posicion la cual será remarcada en roja y será
		  //donde empiece el juego y por tanto se puedan leer movimientos desde las teclas
		  TEntornoActivarCasilla  (fila, col);

		  // Bucle para moverse por el tablero
		  while (!salir) {

			  //Almacena en la variable tecla lo recibido por teclado.
			  //Solo es valido: arriba, abajo, izquierda, derecha, enter y esc.
		       tecla = TEntornoLeerTecla();

		       //cada vez que pulsemo una tecla se actualiza el reloj
		       if (ActualizarReloj(reloj,p,msg)){
		       		//Acorta la barra de tiempo, lo cual se produce cada vez que
		    	   //se lee un movimiento desde el teclado
		    	   TEntornoMedirTiempo(p);
		       		TEntornoMostrarMensaje(Zona1, msg);
		       	}
		       else salir = true; //si se acaba el tiempo el bucle termina

		       switch (tecla) {

				   case TDerecha://tecla derecha

					   //Quita el reslate de la casilla actual para, unas lineas mas abajo resaltar
					   //la casilla siguiente volviendo a invocar a la funcion TEntornoActivarCasilla
					   //el cual tiene como argumentos las coordeadas obtenidas despues de leer por teclado
					   //una direccion.
					   TEntornoDesactivarCasilla(fila, col);
					   if (col < n_columnas-1) col++;
					   TEntornoActivarCasilla(fila, col);
					   TEntornoRefrescarFrontera(n_filas, n_columnas);
					   //dibuja de nuevo la frontera entre los tableros
					   break;

				   case TIzquierda://tecla izquierda

					   TEntornoDesactivarCasilla(fila, col);
					   if (col > 0) col--;
					   TEntornoActivarCasilla(fila, col);
					   TEntornoRefrescarFrontera(n_filas, n_columnas);
					   //dibuja de nuevo la frontera entre los tableros
					   break;

				   case TArriba: //Tecla de arriba

					   TEntornoDesactivarCasilla(fila, col);
					   if (fila > 0) fila--;
					   TEntornoActivarCasilla(fila, col);
					   TEntornoRefrescarFrontera(n_filas, n_columnas);
					   //dibuja de nuevo la frontera entre los tableros
					   break;

				   case TAbajo: //Tecla de abajo

					   TEntornoDesactivarCasilla(fila, col);
					   if (fila < n_filas-1) fila++;
					   TEntornoActivarCasilla(fila, col);
					   TEntornoRefrescarFrontera(n_filas, n_columnas);
					   //dibuja de nuevo la frontera entre los tableros
					   break;

				   case TEnter: //Tecla de enter

					   //Remarca de amarillo la casilla indicada por los argumentos
					   // lo cual significa que será utilizada para intentar emparejarla
					   //y en caso de ser un emparejamiento valido su posterior movimiento.
					   TEntornoMarcarCasilla(fila,col);
					   break;

				   case TSalir://Tecla de escape

					   salir = true;
					   break;

				   default:
					   break;

		       } // Fin del SWITCH

		     } // FIN del WHILE

		if (ganador) {
			TipoMensaje msg = "¡ ¡ E N H O R A B U E N A ! !";
			//Muestra un mensaje en el centro de la interaz, rebordeado por rectangulos
			//indicando que el juego ha concluido. El mensaje presentado está contenido en la cadena msg
			TEntornoMostrarMensajeFin(msg);
		} else {
			TipoMensaje msg = " ¡¡  Inténtalo  otra  vez  !! ";
			TEntornoMostrarMensajeFin(msg);
		}

		TEntornoLeerTecla();

		//Destruye la interfaz gráfica
		TEntornoTerminar();

}

int main () {

	PruebaEntorno();
	return 0;
}



